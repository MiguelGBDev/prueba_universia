const User = require('../mongooseSchemas/User.js');
const Post = require('../mongooseSchemas/Post.js');
//const filteredOptions = {_id: false, __v: false, createdAt: false, updatedAt: false };

/**
 * Insert a new User in database
 */
const _createUser = async (objectQuery) => {
    try {
        const response = await User.create(objectQuery);
        return response;
    } catch (err) {        
        console.log(`[${new Date()}]Failed inserting new Post: ${err}`);
    }    
};

/**
 * Get an user from database with its ID
 */
const _getUser = async (objectQuery) => {
    try {
        const response = await User.findOne({ name: objectQuery.name });//, filteredOptions);
        return response;
    } catch (err) {
        console.log(`[${new Date()}]Failed searching the Post with Id ${objectQuery.PostId}: ${err}`);
    }
};

/**
 * Insert a new User in database
 */
const _login = async (objectQuery) => {
    try {
        const response = await User.findOne(objectQuery);
        return response;
    } catch (err) {        
        console.log(`[${new Date()}]Failed inserting new Post: ${err}`);
    }    
};

/**
 * Insert a new Post in database
 */
const _createPost = async (objectQuery) => {
    try {
        const response = await Post.create(objectQuery);
        return response;
    } catch (err) {        
        console.log(`[${new Date()}]Failed inserting new Post: ${err}`);
    }    
};

/**
 * Get an Post from database with its ID
 */
const _getPost = async (objectQuery) => {
    try {
        const response = await Post.findOne({ _id: objectQuery.postId });
        return response;
    } catch (err) {
        console.log(`[${new Date()}]Failed searching the Post with Id ${objectQuery.PostId}: ${err}`);
    }
};

/**
 * Get all the Posts stored in database
 */
const _getAllPosts = async () => {
    try {
        const response = await Post.find({});
        let parseResponse = {};
        for (const iter in response) {
            const element = response[iter];
            parseResponse[element.PostId] = element;
        }
        return parseResponse;
    } catch (err) {
        console.log(`[${new Date()}]Failed searching the Posts: ${err}`);
    }
};

/**
 * Modify an existing Post
 */
const _modifyPost = async (postId, objectQuery) => {
    try {
        const response = await Post.findOneAndUpdate({ _id: postId }, { message: objectQuery.message }, { new: true });
        return response;
    } catch (err) {
        console.log(`[${new Date()}]Failed modify the Post ${PostId}: ${err}`);
    }
};

/**
 * Delete an Post by its ID
 */
const _removePost = async (objectQuery) => {
    try {
        const response = await Post.findOneAndRemove({ _id: objectQuery.postId });
        return response;
    } catch (err) {
        console.log(`[${new Date()}]Failed deleting the Post ${postId}: ${err}`);
    }
};

module.exports = {
    createUser: _createUser,
    getUser: _getUser,
    login: _login,
    createPost: _createPost,
    getPost: _getPost,
    getAllPosts: _getAllPosts,
    modifyPost: _modifyPost,
    removePost: _removePost
};