const mongoose = require('mongoose');
const dbConfig = require('./mongo_config.json');
const { mongoOpts } = dbConfig.clientData;
let db;

module.exports.DBconnect = () => {
  return new Promise((resolve, reject) => {
    mongoose.Promise = global.Promise;
    if (db) {
      return resolve(db);
    }
    let url = dbConfig.clientData.host;    

    mongoose.connect(url, mongoOpts)
      .then(() => {
        db = mongoose.connection;
        console.log('Mongo Connection Created');
        resolve(db);
      })
      .catch((err) => {
        console.log(`Error creating database connection: ${err}`);
      });
  });
};
