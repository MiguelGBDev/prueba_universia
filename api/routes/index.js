const domain = require('../domain');

/**
 * Create a new User in database
 */
const _register = async (req, res) => {
    let queryData = req.body;

    if (!queryData.name || !queryData.password) {
        res.status(403).send({ Error: 'Name and password are required' });
    } else {
        try {        
            const response = await domain.register(queryData);

            if(response.error) {
                res.status(response.code).send(response);
            } else {
                res.status(200).send(response);
            }        
        } catch (err) {        
            console.log(`[${new Date()}][Routes]Failed creating new User: ${err}`);
            res.status(500).send({ Error: 'Server internal error' });
        }
    } 
};

/**
 * Login an user into the api
 */
const _login = async (req, res) => {
    let queryData = req.body;

    if (!queryData.name || !queryData.password) {
        res.status(403).send({ Error: 'Name and password are required' });
    } else {
        try {        
            const response = await domain.login(queryData);

            if(response.error) {
                res.status(response.code).send(response);
            } else {
                res.status(200).send(response);
            }        
        } catch (err) {        
            console.log(`[${new Date()}][Routes]Failed login an User: ${err}`);
            res.status(500).send({ Error: 'Server internal error' });
        }
    }    
};

/**
 * Insert a new Post in database
 */
const _createPost = async (req, res) => {
    let queryData = req.body;

    if (!queryData.owner || !queryData.message) {
        res.status(403).send({ Error: 'Owner and message are required' });
    } else {
        try {        
            const response = await domain.createPost(queryData);

            if(response.error) {
                res.status(response.code).send(response);
            } else {
                res.status(200).send(response);
            }        
        } catch (err) {        
            console.log(`[${new Date()}][Routes]Failed inserting new Post: ${err}`);
            res.status(500).send({ Error: 'Server internal error' });
        }
    }    
};

/**
 * Get an Post from database with its ID
 */
const _getPost = async (req, res) => {
    let postId = req.params.id;

    try {
        const response = await domain.getPost({ postId });
        if (response.error) {
            res.status(response.code).send(response);
        } else {
            res.status(200).send(response);
        }        
    } catch (err) {
        console.log(`[${new Date()}][Routes]Failed searching the Post with Id ${postId}: ${err}`);
        res.status(500).send({ Error: 'Server internal error' });
    }
};

/**
 * Get all the Posts stored in database
 */
const _getAllPosts = async (req, res) => {
    try {        
        const response = await domain.getAllPosts();
        res.status(200).send(response);
    } catch (err) {
        console.log(`[${new Date()}][Routes]Failed searching the Posts: ${err}`);
        res.status(500).send({ Error: 'Server internal error' });
    }
};

/**
 * Modify an existing Post
 */
const _modifyPost = async (req, res) => {
    let postId = req.params.id;
    let objectQuery = req.body;

    try {
        const response = await domain.modifyPost(postId, objectQuery);
        if (response.error) {
            res.status(response.code).send(response);
        } else {
            res.status(200).send(response);
        }
    } catch (err) {
        console.log(`[${new Date()}][Routes]Failed modify the Post ${postId}: ${err}`);
        res.status(500).send({ Error: 'Server internal error' });
    }
};

/**
 * Delete an Post by its ID
 */
const _removePost = async (req, res) => {
    let postId = req.params.id;
    try {
        const response = await domain.removePost({ postId });
        res.status(200).send(response);
    } catch (err) {
        console.log(`[${new Date()}][Routes]Failed deleting the Post ${postId}: ${err}`);
        res.status(500).send({ Error: 'Server internal error' , text: err});
    }
};

module.exports = {
    register: _register, 
    login: _login,
    createPost: _createPost,
    getPost: _getPost,
    getAllPosts: _getAllPosts,
    modifyPost: _modifyPost,
    removePost: _removePost
}
