const db = require('../db');

/**
 * Insert a new User in database
 */
const _register = async (queryData) => {
	
	let objectQuery = queryData;
	const exists = await db.getUser(objectQuery);	
	if (exists) {
		return { error: 'There is already an user with that name', code: 400 };
	} else {
		const response = await db.createUser(objectQuery);
		return response;
	}	
};

/**
 * Try login an user
 */
const _login = async (queryData) => {
	
	let objectQuery = queryData;
	const exists = await db.getUser(objectQuery);	
	if (!exists) {
		return { error: 'There is not any user with that name', code: 400 };
	} else if (queryData.password === exists.password) {
		return { message: 'The user has been logged correctly', code: 200 };			
	} else {
		return { error: 'Password is incorrect. Please try another time', code: 403 };
	}
};

/**
 * Insert a new Post in database
 */
const _createPost = async (queryData) => {
	
	let objectQuery = queryData;
	
	const response = await db.createPost(objectQuery);
	return response;	
};

/**
 * Get an Post from database with its ID
 */
const _getPost = async (objectQuery) => {
	let { postId } = objectQuery;

	try {
		const response = await db.getPost({ postId });

		if (response === null) {
			return { error: 'There is not any mesage with that id', code: 404 };
		} else {
			return response;
		}
	} catch (err) {
		console.log(`[${new Date()}][Domain]Failed searching the Post with Id ${objectQuery.postId}: ${err}`);
	}
};

/**
 * Get all the Posts stored in database
 */
const _getAllPosts = async () => {
	try {
		const response = await db.getAllPosts();		
		return response;
	} catch (err) {
		console.log(`[${new Date()}][Domain]Failed searching the Posts: ${err}`);
	}
};

/**
 * Modify an existing Post
 */
const _modifyPost = async (postId, objectQuery) => {
	try {
		const exists = await db.getPost({ postId });
        if (!exists) {
            return { error: 'There is not any Post with that PostId', code: 404 };
        } else if (objectQuery.owner === exists.owner) {
			const response = await db.modifyPost(postId, objectQuery);
			return response;
		} else {
			return { error: 'You have not permissions for modify this post', code: 403 };
		}
	} catch (err) {
		console.log(`[${new Date()}][Domain]Failed modify the Post ${postId}: ${err}`);
	}
};

/**
 * Delete an Post by its ID
 */
const _removePost = async (objectQuery) => {
	try {
		const exists = await db.getPost(objectQuery);
        if (!exists) {
            return { error: 'There is not any Post with that PostId', code: 404 };
        } else {
			const response = await db.removePost(objectQuery);
			return response;
		}
	} catch (err) {
		console.log(`[${new Date()}][Domain]Failed deleting the Post ${PostId}: ${err}`);
	}
};

module.exports = {
	register: _register,
	login: _login,
	createPost: _createPost,
	getPost: _getPost,
	getAllPosts: _getAllPosts,
	modifyPost: _modifyPost,
	removePost: _removePost	
};
