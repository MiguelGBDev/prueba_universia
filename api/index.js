/**
 * This program was created by Miguel Ángel Gómez Baena. All rights reserved for him and Open Sistemas SL.
 */
const app = require('./app');
const mongoTools = require('./mongoose_tools'); 
const port = process.env.PORT || 6500;

//  Start server
app.listen(port, () => {
    console.log('Server running on http://localhost:6500');

    mongoTools.DBconnect();
});
