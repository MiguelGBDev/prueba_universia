const express = require ('express');
const app = express();
const router = express.Router();
const bodyParser = require ('body-parser');
const routes = require('./routes');
//const serviceToken = require('./serviceToken');

// Express configuration
app.use(router);
app.use(bodyParser.json());
app.use(bodyParser.text({ limit: '256mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 1000000 }));

//  Definition of the app's routes for USERS
app.post(`/login`,  routes.login);
app.post(`/register`, routes.register);

//  Definition of the app's routes for POSTS
app.post('/post',  routes.createPost);
app.get('/post/:id', routes.getPost);
app.patch('/post/:id', routes.modifyPost);
//app.get('/posts', routes.getAllPost);
app.delete('/post/:id', routes.removePost);

module.exports = app;
