Descripción Prueba: 

A continuación se pasa a describir la API creada para la prueba de Universia:

Esta api permite el registro y login de usuarios, a la vez que permite gestionar sus posts enviados.
Para la capa de persistencia he utilizado una BBDD MongoDB con dos colecciones, una para usuarios y otra para post.

Me disculpo de antemano, no me ha dado tiempo a crear un contenedor Docker para la aplicación.


PARA INICIAR LA APLICACION:
cd prueba_tecnica_universia/api
npm install
node index.js

La API esta escuchando a través del puerto 6500

Las rutas o endpoints que usa la API son:

Para registrar usuarios
    localhost:6500/register, metodo POST 
    Campos requeridos: name, password
    Ejemplo: 
    curl -X POST \
    http://localhost:6500/register \
    -H 'Cache-Control: no-cache' \
    -H 'Content-Type: application/json' \
    -H 'Postman-Token: 73ac32a5-4ed9-47b6-9581-5db3ed3f8907' \
    -d '{
        "name": "pepe",
        "password": "pepe"
    }'

Para loguear usuarios
    localhost:6500/login, metodo POST 
    Campos requeridos: name, password
    Ejemplo:
    curl -X POST \
    http://localhost:6500/login \
    -H 'Cache-Control: no-cache' \
    -H 'Content-Type: application/json' \
    -H 'Postman-Token: e68c2d98-65ae-4b11-9343-3df95b0b077e' \
    -d '{
        "name": "pepe",
        "password": "pepe"
    }'

Para crear posts
    localhost:6500/post,  Método  POST
    Campos requeridos: Owner, message
    Ejemplo:
    curl -X POST \
    http://localhost:6500/post \
    -H 'Cache-Control: no-cache' \
    -H 'Content-Type: application/json' \
    -H 'Postman-Token: 616e45cd-3c29-4405-bd8a-088235945047' \
    -d '{
        "owner": "pepe",
        "message": "Este es el primer mensaje de pepe"
    }'

Para Obtener un Post sabiendo su ID:
    localhost:6500/post/:id Método GET
    Campos Requeridos: ID
    Ejemplo: 
    curl -X GET \
    http://localhost:6500/post/5cf0ffd3910a3628d5fe39f3 \
    -H 'Cache-Control: no-cache' \
    -H 'Postman-Token: 7b3dd4cb-0c3b-450d-98bc-e7ce695ab931'

Para Modificar un Post sabiendo su ID:
    localhost:6500/post/:id Método PATCH
    Campos Requeridos: ID, Owner
    Ejemplo: 
    curl -X PATCH \
    http://localhost:6500/post/5cf0ffd3910a3628d5fe39f3 \
    -H 'Cache-Control: no-cache' \
    -H 'Content-Type: application/json' \
    -H 'Postman-Token: fe35c0b5-a137-4ce0-9b6e-0e729a3f63d9' \
    -d '{
        "owner": "pepe",
        "message": "Este es el tercer mensaje de pepe"
    }'

Para Borrar un Post sabiendo su ID:    
    localhost:6500/post/:id Método DELETE
    Campos Requeridos: ID, Owner
    Ejemplo: 
    curl -X DELETE \
    http://localhost:6500/post/5cf0ffd3910a3628d5fe39f3 \
    -H 'Cache-Control: no-cache' \
    -H 'Postman-Token: d4c6b7e8-430d-4846-a86e-40c8955a0c09'

Un saludo,
Miguel